# Descomente esto si da error
 #import nltk
 #nltk.download('stopwords')
 #nltk.download('punkt')
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from collections import defaultdict
from formulas import Formula
from indexacion import Indexacion
from sklearn.metrics.pairwise import cosine_similarity
import numpy as np
from random import choice

class Buscador:
    consulta = ""
    terminos_consulta = []
    indice_invertido = {}
    indice = dict()
    documentos_entrelazados = []
    k = 20 #Cuantos documentos va a retornar

    def __init__(self, ind_inv):
        self.indice_invertido = ind_inv
    
    def realizar_busqueda(self, consulta):
        """
        Función: Función principal del buscador
        E: String (consulta del usuario)
        S: lista con los k documentos más relevantes para esa consulta
        """
        self.indice = defaultdict(list)
        self.consulta = consulta
        self.extraer_terminos()
        self.documentos_por_termino()
        self.entrelazar_documentos()
        # self.clasificar_resultados()
        if len(self.documentos_entrelazados) < self.k:
            return self.documentos_entrelazados
        return self.documentos_entrelazados[:self.k]

    def extraer_terminos(self):
        """
        Función: Convierte la consulta en una lista de términos
        E: N/A
        S: N/A
        """
        terminos = word_tokenize(self.consulta.lower())
        stopwords_español = stopwords.words('spanish')
        self.terminos_consulta = [word for word in terminos if not word in stopwords_español]

    def documentos_por_termino(self):
        """
        Función: Crea un diccionario, donde la llave es un documento, y el valor
            es una lista que contiene [AparicionesTérmino, Término]
        E: N/A
        S: N/A
        """
        for termino in self.terminos_consulta:
            if termino not in self.indice_invertido:
                continue
            documentos = sorted(self.indice_invertido[termino], reverse=True)
            for cantidad, documento in documentos:
                self.indice[documento].append([cantidad, termino])
            
        while len(self.indice.keys()) < self.k:
            tmp_term = choice(list(self.indice_invertido.keys()))
            if tmp_term in self.terminos_consulta:
                continue
            documentos = sorted(self.indice_invertido[tmp_term], reverse=True)
            for cantidad, documento in documentos:
                self.indice[documento] = [[0, tmp_term]]
    
    def entrelazar_documentos(self):
        """
        Función: Entrelaza los documentos de cada término, ordena una lista
            por la suma de las apariciones de cada término, y después por
            la cantidad de términos de la consulta en ese documento
        E: N/A
        S: N/A
        """
        documentos_ordenados = []
        for documento in self.indice.keys():
            documentos_ordenados.append(self.indice[documento] + [documento])
        self.documentos_entrelazados = sorted(documentos_ordenados, reverse=True, key=sumar_apariciones)
        self.documentos_entrelazados.sort(reverse=True, key=len)

    def clasificar_resultados(self):
        """
        Función: Retorna el top k de direcciones después de calcular cuáles son más importantes
        E: N/A
        S: Las direcciones con más relevancia para la búsqueda
        """
        lista_similitudes = []
        contador = 0
        for termino in self.terminos_consulta:
            if termino not in self.indice_invertido:
                contador += 1
                continue
            cota_inferior, lista_documentos, lista_pesos_consulta = self.buscar_cota_inferior(termino)
            for i in range(len(lista_documentos)):
                lista_pesos_termino_documento = self.peso_termino_documento(termino, lista_documentos[i])
                lista_pesos_documento = self.listar_pesos_documento(lista_documentos[i])
                if(lista_pesos_documento[contador] < cota_inferior):
                    continue
                vector_1 = np.array([lista_pesos_documento])
                vector_2 = np.array([lista_pesos_termino_documento])
                similitud = cosine_similarity(vector_1, vector_2)
                lista_similitudes.append(similitud)
                
            contador += 1
        return None
    
    def buscar_cota_inferior(self, termino):
        """
        Función: Retorna la cota inferior de los pesos de los términos de una página web
        E: Un término
        S: El menor número encontrado en el diccionario
        """
        lista_documentos = []
        lista_pesos = []
        for i in range(len(self.indice_invertido[termino])):
            lista_documentos += [self.indice_invertido[termino][i][1]]
        for j in range(len(lista_documentos)):
            formula = Formula(termino, lista_documentos[j])
            lista_pesos += [formula.wij]
        cota_inferior = min(lista_pesos)

        return cota_inferior, lista_documentos, lista_pesos
    
    def peso_termino_documento(self, termino, documento):
        """
        Función:
        E: un término y un documento
        S: una lista con los pesos del termino en el documento
        """
        lista_pesos = []
        for term in self.terminos_consulta:
            if term == termino:
                formula = Formula(termino, documento)
                lista_pesos.append(formula.wij)
            else:
                lista_pesos.append(0)
        return lista_pesos

    
    def listar_pesos_documento(self, documento):
        """
        Función: Dado un término, se crea una lista de las direcciones que contengan este término
        E: Un término a buscar
        S: Una lista con las direcciones que contienen el término
        """
        lista_pesos = []
        for termino in self.terminos_consulta:
            bandera = False
            for _, term in self.indice[documento]:
                if termino == term:
                    formula = Formula(termino, documento)
                    lista_pesos.append(formula.wij)
                    bandera = True
                    break
            if not(bandera):
                lista_pesos.append(0)
        return lista_pesos
    

def sumar_apariciones(documentos):
    """
    Función: Retorna la suma de la cantidad de apariciones de los 
        términos de la consulta en un documento
    E: N/A
    S: N/A
    """
    total = 0
    for cantidad, _ in documentos[:-1]:
        total += cantidad
    return total
    