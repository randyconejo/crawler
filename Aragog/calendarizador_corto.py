from email import header
from socket import setdefaulttimeout
from urllib.request import Request, urlopen
import ssl
import certifi
from bs4 import BeautifulSoup

class CalendarizadorCorto:
    url_base = ''
    soup = None
    no_permitidas = []
    user_agent = ""
    
    def definir_tiendas(self, url, header):
        """
        Función que define las diferentes tiendas en el proceso de arañado
        E: una dirección a una página web y un encabezado con el User-Agent
        S: Ninguna
        """
        self.url_base = url
        self.user_agent = header['User-Agent']
        self.robot_url = url + "robots.txt"
        respuesta = Request(self.robot_url, headers=header)
        pagina_web = urlopen(respuesta, context=ssl.create_default_context(cafile=certifi.where()))
        self.soup = BeautifulSoup(pagina_web.read(), "html.parser")
    
    def obtener_no_permitidas(self):
        """
        Función que que se encarga de obtener los links no permitidos de cada página
        E: Ninguna
        S: Ninguna
        """
        lineas = self.separar_agentes()
        for linea in lineas:
            if linea.startswith("Disallow:"):
                split = linea.split(":", maxsplit=1)
                self.no_permitidas.append(split[1].strip())
        self.limpiar_no_permitidas()

    def separar_agentes(self):
        """
        Función que se encarga de separar los User-Agents del robots.txt
        E: Ninguna
        S: Ninguna
        """
        lineas = str(self.soup).splitlines()
        i = 0
        inicio = 0
        final = len(lineas)
        inicio_conseguido = False
        while i < len(lineas):
            if lineas[i][:12].upper() == 'USER-AGENT: ':
                if lineas[i][:13 + len(self.user_agent)].upper() == 'USER-AGENT: ' + self.user_agent.upper():
                    inicio = i
                    inicio_conseguido = True
                else:
                    if inicio_conseguido:
                        final = i
                        break
            i += 1
        return lineas[inicio:final]


    def limpiar_no_permitidas(self):
        """
        Función que limpia los Disallows
        E: Ninguna
        S: Ninguna
        """
        i = 0
        while i < len(self.no_permitidas):
            if self.no_permitidas[i][:2] == "/*":
                self.no_permitidas[i] = self.no_permitidas[i][2:]
            elif self.no_permitidas[i][:2] == "*/":
                self.no_permitidas[i] = self.no_permitidas[i][1:]
            i += 1
    
    def eliminar_no_permitidas(self, links_pendientes):
        """
        Función que elimina los Disallows de la lista que le entre
        E: Una lista con links
        S: Ninguna
        """
        for i in links_pendientes:
            for j in self.no_permitidas:
                if j in i:
                    links_pendientes.remove(i)

    def enlace_valido(self, direccion):

        if "carrito" in direccion or "carro" in direccion or direccion.endswith("jpg")\
            or "wa.me" in direccion or "whatsapp" in direccion or "facebook" in direccion\
            or "instagram" in direccion or "messenger" in direccion or "email" in direccion\
            or "protected in direccion" in direccion or "wishlist" in direccion or "cart" in direccion\
            or "tiktok" in direccion or "youtube" in direccion or "waze" in direccion:

            return False
        
        else:
            return True