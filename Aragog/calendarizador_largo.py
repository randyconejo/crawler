from datetime import datetime, timedelta
import json
from jsonpath import jsonpath

class CalendarizadorLargo:
    direcciones = []
    fecha_actual_texto = ""
    fecha_actual = None
    politicas = {}
    tiendas = {}
    archivo = None
    visita_concurrencia = 0
    profundidad = 0
    #descarga_concurrencia = 0
    #descarga_dominio_concurrencia = 0
    #bytes_concurrencia = 0

    def __init__(self):
        self.archivo = open("Aragog/resources/politicas.json","r")
        self.politicas = json.loads(self.archivo.read())
        self.fecha_actual_texto = datetime.today().strftime('%d/%m/%Y')
        self.fecha_actual = datetime.strptime(self.fecha_actual_texto, '%d/%m/%Y')
        self.tiendas = jsonpath(self.politicas, "visitacion")[0]
        self.politicas_seleccion = jsonpath(self.politicas, "seleccion")[0]
        self.obtener_politicas_seleccion()
    
    def calendarizar(self):
        """
        Función que se encarga de indicar cuáles url's deben ser visitados
        E: Un número que indica la cantidad de hilos que se van a utilizar
        S: Una lista con los url's que se tienen que visitar
        """
        #Claramente estos dos tienen valores random porque no tengo idea de cuales usar
        MAX_DIRECCIONES = self.visita_concurrencia

        for tienda in self.tiendas.keys():
            if len(self.direcciones) < MAX_DIRECCIONES:
                revisitacion = jsonpath(self.tiendas, f"{tienda}.revisitacion")[0]
                fecha_revisitacion = datetime.strptime(revisitacion, '%d/%m/%Y')
                if fecha_revisitacion <= self.fecha_actual:
                    self.direcciones.append(jsonpath(self.tiendas, f"{tienda}.link")[0])
                    self.actualizar_numero_visitas(tienda)
                    self.actualizar_ultima_visita(tienda)
                    self.actualizar_revisitacion(tienda)
                    file = open("Aragog/resources/politicas.json", "w")
                    politicas_modificadas = json.dumps(self.politicas, indent = 4)
                    file.write(politicas_modificadas)
            else:
                break

    
    def actualizar_ultima_visita(self, local):
        """
        Función que actualiza la última visita del archivo json
        E: un diccionario
        S: Ninguna
        """
        contenido_local = jsonpath(self.tiendas, f"{local}")[0]
        for elem in contenido_local:
            if elem == "ultimaVisita":
                contenido_local["ultimaVisita"] = self.fecha_actual_texto

    def actualizar_numero_visitas(self, local):
        """
        Función que actualiza el numero de visitas de un archivo json
        E: un diccionario
        S: Ninguna
        """
        contenido_local = jsonpath(self.tiendas, f"{local}")[0]
        for elem in contenido_local:
            if elem == "numVisitas":
                contenido_local["numVisitas"] = contenido_local["numVisitas"] + 1

    def actualizar_revisitacion(self, local):
        """
        Función que actualiza la revisitación de un archivo json
        E: un diccionario
        S: Ninguna
        """
        contenido_local = jsonpath(self.tiendas, f"{local}")[0]
        for elem in contenido_local:
            if elem == "revisitacion":
                contenido_local["revisitacion"] = (self.fecha_actual + timedelta(7)).strftime('%d/%m/%Y')

    def obtener_politicas_seleccion(self):
        """
        Función que se encarga de obtener las políticas de selección del diccionario politicas_seleccion.
        E: el diccionario con las politicas de selección.
        S: Ninguna, solo crea las variables para cada política respectiva.
        """
        self.visita_concurrencia = int(self.politicas_seleccion['visitaConcurrencia'])
        self.profundidad = int(self.politicas_seleccion['profundidad'])
