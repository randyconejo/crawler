from bs4 import BeautifulSoup
from urllib.request import Request, urlopen
import ssl
import certifi

class Descargador:
    soup = None
    url_base = ''
    direcciones = []

    def __init__(self, url, header):
        self.url_base = url
        respuesta = Request(self.url_base, headers=header)
        pagina_web = urlopen(respuesta, context=ssl.create_default_context(cafile=certifi.where()))
        self.soup = BeautifulSoup(pagina_web.read(), "html.parser")
    
    def obtener_enlaces(self):
        """
        Función que obtiene los enlaces
        E: Ninguna
        S: Ninguna
        """
        enlaces = self.soup.find_all("a")
        
        for enlace in enlaces:
            
            try:
                direccion = enlace["href"]
                if direccion[0] == '/':
                    self.direcciones.append(self.url_base + direccion[1:])
                if direccion[:5] == 'https':
                    self.direcciones.append(direccion)

            except:
                continue

    def obtener_informacion(self, string, url):
        """
        Función que obtiene la información relevante de un string
        E: Ninguna
        S: Ninguna
        """
        
        if "adn" in url:
            return ["Vacio"]
        
        elif "cyberteamcr" in url:
            return self.obtener_informacion_aux("XXXXXXXXXXXXXXXXXX", string)

        elif "extremetech" in url:
            return self.obtener_informacion_aux("Productos Relacionados", string)
        
        elif "grupocreta" in url:
            return self.obtener_informacion_aux("Productos relacionados", string)
        
        elif "intelec" in url:
            return self.obtener_informacion_aux("Tambien le puede interesar", string)
        
        elif "pcgamingcr" in url:
            return ["Vacio"]
        
        elif "ticotek" in url:
            return self.obtener_informacion_aux("Productos relacionados", string)
        
        else:
            return ["Vacio"]

    def obtener_informacion_aux(self, separador, string):
        """
        Función auxiliar para obtener la lista de términos
        E: Un separador para split() y un string a parsear
        S: La lista de términos
        """

        terminos = string.split(separador)
        nuevos_terminos = terminos[0].lower().split()
        return nuevos_terminos