import json
import math
import pathlib
import os


class Formula:
    fij = 0
    tf = 0
    idf = 0
    wij = 0

    def __init__(self, termino, ruta):
        archivo = open("Aragog/resources/indice/index.json","r")
        self.contenido = json.loads(archivo.read())
        self.termino = termino
        self.ruta = ruta
        if self.termino in self.contenido.keys():
            self.apariciones_termino = self.contenido[self.termino]
            self.obtener_fij()
            self.obtener_tf()
            self.obtener_idf()
            self.obtener_wij()

    def obtener_fij(self):
        """
        Función: Retorna el fij calculado
        E: N/A
        S: N/A
        """
        for documento in self.apariciones_termino:
            if documento[1] == self.ruta:
                self.fij = documento[0]

    def obtener_tf(self):
        """
        Función: Retorna el tf calculado
        E: N/A
        S: N/A
        """
        if self.fij != 0:
            self.tf = 1 + math.log2(self.fij)
    
    def obtener_idf(self):
        """
        Función: Retorna el idf calculado
        E: N/A
        S: N/A
        """
        N = 0
        tiendas = ["adn", "cyberteamcr", "extremetech", "intelec", "pcgamingcr", "ticotek"]
        for tienda in tiendas:
            for archivo in pathlib.Path(f"Aragog/resources/almacen/{tienda}").iterdir():
                if archivo.is_file():
                    N += 1
        ni = len(self.apariciones_termino)
        self.idf = math.log2(N/ni)
    
    def obtener_wij(self):
        """
        Función: Retorna el wij calculado
        E: N/A
        S: N/A
        """
        self.wij = self.idf * self.tf

    def guardar_binario(self):
        """
        Función: Guarda los datos en binario
        E: N/A
        S: N/A
        """
        nombre = self.ruta.split("/")
        nombre = nombre[-2] + "-" + nombre[-1][:-4]
        ruta = os.path.join('resources', 'binarios', f"{self.termino}-{nombre}.bin")
        archivo = open(ruta, "wb")
        lista_datos = f"{self.fij}, {self.tf}, {self.idf}, {self.wij}"
        datos_binarios = bytes(lista_datos, encoding = "utf-8")
        archivo.write(datos_binarios)
        archivo.close()
    
    def leer_binario(self, ruta):
        """
        Función: Lee los datos en binario
        E: N/A
        S: N/A
        """
        archivo = open(ruta, "rb")
        datos = archivo.read().decode()
        datos = datos.split(",")
        datos = [float(elem) for elem in datos]
        res = {
            "fij" : datos[0],
            "tf" : datos[1],
            "idf" : datos[2],
            "wij" : datos[3]
        }
        return res
