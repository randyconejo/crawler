import json


class Historial:
    relevantes_5 = 0
    relevantes_10 = 0
    relevantes_20 = 0
    lista_res = []
    historial = {}
    consulta = ""


    def __init__(self, lista_res, consulta):
        archivo = open("Aragog/resources/historial.json","r")
        self.historial = json.loads(archivo.read())
        self.lista_res = lista_res
        self.consulta = consulta
        self.contar_relevantes()
        self.guardar_busqueda()


    def contar_relevantes(self):
        """
        Función: Cálcula el p@5
        E: N/A
        S: Un entero que indica la precisión en un punto fijo
        """
        ind = 0
        while ind < len(self.lista_res):
            if self.lista_res[ind] == True:
                if ind < 5:
                    self.relevantes_5 += 1
                    self.relevantes_10 += 1
                    self.relevantes_20 += 1
                elif ind < 10:
                    self.relevantes_10 += 1
                    self.relevantes_20 += 1
                else:
                    self.relevantes_20 += 1
            ind += 1

    
    def calcular_P5(self):
        """
        Función: Cálcula el p@5
        E: N/A
        S: Un entero que indica la precisión en un punto fijo
        """
        return self.relevantes_5/5
    

    def calcular_P10(self):
        """
        Función: Cálcula el p@10
        E: N/A
        S: Un entero que indica la precisión en un punto fijo
        """
        return self.relevantes_10/10
    

    def calcular_P20(self):
        """
        Función: Cálcula el p@20
        E: N/A
        S: Un entero que indica la precisión en un punto fijo
        """
        return self.relevantes_20/20


    def guardar_busqueda(self):
        """
        Función: Guarda la búsqueda realizada por el usuario y sus respectivas métricas
        E: N/A
        S: N/A
        """
        nueva_busqueda = {
            "consulta": self.consulta,
            "P5": self.calcular_P5(),
            "P10": self.calcular_P10(),
            "P20": self.calcular_P20()
        }

        lista_actualizada = self.historial["historial"]
        lista_actualizada.append(nueva_busqueda)
        self.historial["historial"] = lista_actualizada
        archivo = open("Aragog/resources/historial.json", "w")
        historial_actualizado = json.dumps(self.historial, indent = 4)
        archivo.write(historial_actualizado)
    
    def limpiar_historial(self):
        """
        Función: Borra el historial
        E: N/A
        S: N/A
        """
        historial_vacio = {
            "historial": [
            ]
        }
        archivo = open("Aragog/resources/historial.json", "w")
        historial_vacio = json.dumps(historial_vacio, indent = 4)
        archivo.write(historial_vacio)
