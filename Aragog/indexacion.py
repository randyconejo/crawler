import pathlib
import json
from formulas import Formula
from collections import defaultdict
import pickle

class Indexacion:
    
    #Diccionario de directorios
    dir = ['Aragog/resources/almacen/adn',
    'Aragog/resources/almacen/cyberteamcr',
    'Aragog/resources/almacen/extremetech',
    'Aragog/resources/almacen/intelec',
    'Aragog/resources/almacen/pcgamingcr',
    'Aragog/resources/almacen/ticotek']

    #Diccionario de terminos
    tabla_dispersion = {}
    indice_bueno = []
    indice_docs = defaultdict(list)
    
    #Diccionario de archivos
    archivos_visitados = {
        "adn" : [],
        "cyberteamcr" : [],
        "extremetech" : [],
        "intelec" : [],
        "pcgamingcr" : [],
        "ticotek" : []
    }
    
    
    def inicio_index (self):
        """
        Función: Inicializar nuestro indice y verifica documentos json
        E: Ninguna
        S: Ninguna
        """
        index_json = pathlib.Path('Aragog/resources/indice/index.json')
        archivos_json = pathlib.Path('Aragog/resources/indice/archivos.json')
        index_bin = pathlib.Path('Aragog/resources/indice/index.bin')

        if index_json.exists():
            with open(index_json, 'r') as fp:
                self.tabla_dispersion = json.load(fp)
        else:
            with open(index_json, 'w') as fp:
                json.dump(self.tabla_dispersion, fp) 
        
        if archivos_json.exists():
            with open(archivos_json, 'r') as fa:
                self.archivos_visitados = json.load(fa)
        else:
            with open(archivos_json, 'w') as fa:
                json.dump(self.archivos_visitados, fa)
        
        if index_bin.exists():
            with open(index_bin, 'rb') as ib:
                self.indice_bueno = pickle.load(ib)

        for i in self.dir:
            pag = i.split("/")
            cambios = 0
            directorio = pathlib.Path(i)
            archivos_visitados = self.archivos_visitados[pag[-1]]
            for fichero in directorio.iterdir():
                if (i + "/" + fichero.name) not in archivos_visitados:
                    self.datos_txt(i + "/" + fichero.name)
                    temp = self.archivos_visitados[pag[-1]]
                    temp.append(i + "/" + fichero.name)
                    cambios = 1

            if cambios == 1:
                self.archivos_visitados[pag[-1]] = temp

        with open(archivos_json, 'w') as fa:
            json.dump(self.archivos_visitados, fa)

        with open(index_json, 'w') as fp:
            json.dump(self.tabla_dispersion, fp) 

        """
        if not index_bin.exists():
            self.crear_indice_bueno()
        """

    def crear_indice_bueno(self):
        """
        Función: Crea el vector para cada documento
        E: Ninguna
        S: Ninguna
        """
        print("Creando indice de docs")
        for termino in self.tabla_dispersion.keys():
            documentos = self.tabla_dispersion[termino]
            for _, documento in documentos:
                self.indice_docs[documento].append(termino)

        # Primera fila con los términos
        self.indice_bueno.append(list(self.tabla_dispersion))
        for ruta in self.dir:
            directorio = pathlib.Path(ruta)
            for fichero in directorio.iterdir():
                nombre_archivo = ruta + "/" + fichero.name
                print("Analizando " + nombre_archivo)
                vector = [nombre_archivo]
                for termino in self.indice_bueno[0]:
                    if termino not in self.indice_docs[nombre_archivo]:
                        vector.append(0)
                        continue
                    formula = Formula(termino, nombre_archivo)
                    vector.append(formula.wij)
                self.indice_bueno.append(vector)


    def datos_txt(self,archivo):
        """
        Función: Carga los términos de los documentos txt
        E: String (Ruta del archivo)
        S: Ninguna
        """
        with open(archivo, encoding="utf-8") as f:
            mensaje = f.readlines()[4].rstrip()
        
        lista_datos = mensaje.split(",") 

        self.asignar_index(lista_datos,archivo)


    def asignar_index(self,terminos,nombre_doc):
        """
        Función: Realiza el indice invertido guardandolo en un diccionario
        E: String (Terminos del txt), String (Nombre del documento)
        S: Ninguna
        """
        terminos.sort()
        for i in terminos:
            if i not in self.tabla_dispersion:
                self.tabla_dispersion[i] = [[1,nombre_doc]]
            if i in self.tabla_dispersion:
                lista_doc = self.tabla_dispersion[i]
                h=0
                bandera = 0
                while h < len(lista_doc):
                    if nombre_doc == lista_doc[h][1]:
                        bandera = 1
                        lista_doc[h][0] += 1
                        break
                
                    h += 1
                if bandera == 0:
                    lista_doc.append([1,nombre_doc])

                self.tabla_dispersion[i] = lista_doc

if __name__ == "__main__":
    indexador = Indexacion()
    indexador.inicio_index()
