from flask import Flask, render_template, request
from modulo_control import Controlador
from historial import Historial
import json

app = Flask(__name__)
controlador = Controlador()
controlador.indexar()
historial = {}

@app.route("/")
def main():
    return render_template('index.html')


@app.route('/buscar_consulta', methods=['POST'])
def buscar_consulta():
    consulta = request.form['consulta']
    links = controlador.buscar(consulta)
    return render_template('respuesta.html', links=links)

@app.route('/guardar_historial', methods=['POST'])
def guardar_historial():
    docs_relevantes = request.form.getlist('docs_relevantes')
    lista_booleanos = []
    for i in range(0, 20):
        lista_booleanos.append(str(i) in docs_relevantes)
    Historial(lista_booleanos, controlador.buscador.consulta)
    return render_template('index.html')

@app.route('/ver_historial')
def ver_historial():
    with open("Aragog/resources/historial.json", 'r') as hs:
        historial = json.load(hs)
    largo_historial = len(historial["historial"])
    if largo_historial == 0:
        return render_template('historial_vacio.html')
    lista_historial = []
    for consulta in historial["historial"]:
        nuevo_historial = [consulta["consulta"], consulta["P5"], consulta["P10"], consulta["P20"]]
        lista_historial.append(nuevo_historial)
        
    return render_template("historial.html", lista_historial=lista_historial)

@app.route('/limpiar_historial')
def limpiar_historial():
    historial["historial"] = []
    with open("Aragog/resources/historial.json", 'w') as hs:
        json.dump(historial, hs)
    
    return render_template('historial_vacio.html')

if __name__ == "__main__":
    app.run()