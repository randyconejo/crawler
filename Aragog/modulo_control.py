from asyncio import futures
from calendarizador_corto import CalendarizadorCorto
from calendarizador_largo import CalendarizadorLargo
from normalizador import Normalizador
from descargador import Descargador
from preprocesamiento import Preprocesamiento
from indexacion import Indexacion
from buscador import Buscador
import urllib


class Controlador:
    calendarizador_largo = None
    calendarizador_corto = None
    normalizador = None
    preprocesamiento = None
    indexador = None
    buscador = None

    def __init__(self):
        self.calendarizador_largo = CalendarizadorLargo()
        self.calendarizador_corto = CalendarizadorCorto()
        self.normalizador = Normalizador()
        self.indexador = Indexacion()
        self.buscador = Buscador(None)

    def arannar(self):
        """
        Función que se encarga de realizar el proceso principal del arañado
        E: Ninguna
        S: Ninguna
        """
        while True:
            self.enlaces_visitados = []
            self.calendarizador_largo.direcciones = []
            #Se le pide al calendarizador largo calendarizar
            self.calendarizador_largo.calendarizar()
            direcciones = self.calendarizador_largo.direcciones
            #Se valida el encabezado para cada link, en este caso, solo adntienda necesita un encabezado distinto
            encabezados = [{'User-Agent': (lambda direccion: 'GoogleBot' if (direccion=='https://www.adntienda.com/') else '*')(direccion)} for direccion in direcciones]
            for direccion, encabezado in zip(direcciones, encabezados):
                self.calendarizador_corto.definir_tiendas(direccion,encabezado)
            self.calendarizador_corto.obtener_no_permitidas()

            self.arannar_aux(direccion, encabezado, 0)

            self.normalizador.contador_archivo = 0

    def arannar_aux(self, direccion, encabezado, nivel_actual):
        """
        Función auxiliar de arannar, se encarga de realizar un proceso recursivo de arañamiento por niveles
        E: una dirección a una página web, el encabezado con el User-Agent de la página y el nivel de profundidad de la recursión
        S: Ninguna
        """

        profundidad = self.calendarizador_largo.profundidad
        print("Dirección: ", direccion)
        print("Nivel actual: ", nivel_actual)
        
        if self.calendarizador_corto.enlace_valido(direccion):
        
            if (nivel_actual < profundidad) and self.normalizador.contador_archivo != 300:
                if(direccion not in self.enlaces_visitados):

                    direccion = self.normalizador.quitar_tildes(direccion)
                    self.enlaces_visitados.append(direccion)

                    try:
                        descargador = Descargador(direccion, encabezado)
                        descargador.direcciones = [] #borrar
                        
                        descargador.obtener_enlaces()
                        enlaces = descargador.direcciones
                        
                        enlaces = [self.normalizador.quitar_tildes(enlace) for enlace in enlaces]
                            
                        self.calendarizador_corto.eliminar_no_permitidas(enlaces)

                        html_pagina = descargador.soup.get_text()
                        html_limpio = self.normalizador.limpieza(html_pagina)

                        terminos = descargador.obtener_informacion(html_limpio, direccion)
                        terminos_limpios = self.normalizador.limpiar_terminos(terminos)
                        self.normalizador.guardar_informacion(direccion, html_limpio, terminos_limpios)
                        print("Se guardó exitosamente, continuando...")
                        
                        for enlace in enlaces:
                            self.arannar_aux(enlace, encabezado, nivel_actual + 1)

                    except urllib.error.HTTPError as e:
                        print(e)

                    return None
                else:
                    print("Saltando dirección repetida")
                    return None
            else:
                print('\nSe termino de arañar: ' + direccion)
                return None
        
        else:
            print("Saltando dirección inválida")
            return None

    def indexar(self):
        """
        Función que se encarga de realizar el proceso de indexación
        E: Ninguna
        S: Ninguna
        """
        self.indexador.inicio_index()

    def obtener_links(self, documentos_relevantes):
        links = [self.buscador.consulta]
        for doc in documentos_relevantes:
            with open(doc[-1], "r", encoding='utf-8') as f:
                link = f.readline().replace("\n", "")
                links.append(link)
                
        return links

    def buscar(self, consulta):
        """
        Función que se encarga de realizar el proceso de búsqueda dada una consulta
        E: Un string que corresponde a una consulta
        S: Ninguna
        """
        self.buscador.indice_invertido = self.indexador.tabla_dispersion
        documentos_relevantes = self.buscador.realizar_busqueda(consulta)

        print("Resultados")
        for doc in documentos_relevantes:
            print(doc[-1])
        
        return self.obtener_links(documentos_relevantes)
        

if __name__ == "__main__":
    controlador = Controlador()
    #controlador.arannar()
    controlador.indexar()
    controlador.buscar("RTX")