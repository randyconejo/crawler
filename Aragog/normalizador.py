from xml.etree.ElementTree import tostring
from descargador import Descargador
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
import os
import sys
import nltk
#nltk.download('stopwords')

class Normalizador:

    contador_archivo = 1

    
    def limpiar_informacion(self, informacion):
        """
        Función que se encarga de limpiar la información entrante
        E: Una lista con strings de información
        S: retorna la información limpia
        """

        informacion_limpia = []

        for texto in informacion:
            informacion_limpia.append(texto.replace("\t", "").replace("\n", "").strip())

        informacion_limpia = [texto for texto in informacion_limpia if texto[-3:] != "..."]

        informacion_limpia = list(set(self.remover_elementos(informacion_limpia, "")))

        return informacion_limpia

    def remover_elementos(self, elementos, valor):
        """
        Función que se encarga de remover un valor dado de una lista de elementos
        E: Una lista y un valor a eliminar
        S: Una lista sin el valor dado
        """
        elementos_nuevos = [elemento for elemento in elementos if elemento != valor]
        return elementos_nuevos

    def limpieza(self, string):
        """
        Función que se encarga de limpiar un string
        E: String a limpiar
        S: String limpio
        """
        return string.replace("\n", " ").replace("'", "").replace('"', '')\
                            .replace("/", " ").replace("|", " ")\
                            .replace("ñ", "nn")\
                            .replace("á", "a")\
                            .replace("é", "e")\
                            .replace("í", "i")\
                            .replace("ó", "o")\
                            .replace("ú", "u")\
                            .replace("₡", "C")\
                            .replace(">", " ")\
                            .replace("│", "")\
                            .replace('″', "")\
                            .strip()

    def quitar_tildes(self, string):
        """
        Función que se encarga de reemplazar tildes en un string
        E: String a limpiar
        S: String sin tildes
        """
        return string.replace("á", "a")\
                    .replace("é", "e")\
                    .replace("í", "i")\
                    .replace("ó", "o")\
                    .replace("ú", "u")

    def limpiar_terminos(self, terminos):
        """
        Función que se encarga de limpiar una lista de términos
        E: Una lista de términos
        S: Un string con los términos separados por comas
        """

        terminos_string = ','.join([str(termino) for termino in terminos])
        terminos_string_limpios = self.limpieza(terminos_string)
        puntuacion = nltk.RegexpTokenizer(r"\w+")
        nuevos_terminos = puntuacion.tokenize(terminos_string_limpios)
        stopwords_español = stopwords.words('spanish')
        terminos_limpios = [word.lower() for word in nuevos_terminos if not word in stopwords_español]
        resultado = ','.join([str(termino) for termino in terminos_limpios])
        return resultado

    def guardar_informacion(self, url, html, terminos):
        """
        Función que se encarga de guardar información en un txt
        E: Un url, un html limpio y un string de términos limpios
        S: Ninguna
        """
        
        NUMERO_ARCHIVO = self.contador_archivo

        almacen = self.obtener_almacen(url)
        path = os.path.join('Aragog', 'resources', 'almacen', almacen, 'tienda'+ str(NUMERO_ARCHIVO) +'.txt')
        
        with open(path, 'w+', encoding="utf-8") as archivo:
            archivo.write(str(url) + "\n"+ html + '\n')
            archivo.write('\nLista de Términos: \n')
            archivo.write(terminos)

        self.contador_archivo += 1
    
    def obtener_almacen(self, url):

        if "adn" in url:
            return "adn"
        
        elif "cyberteamcr" in url:
            return "cyberteamcr"

        elif "extremetech" in url:
            return "extremetech"
        
        elif "grupocreta" in url:
            return "grupocreta"
        
        elif "intelec" in url:
            return "intelec"
        
        elif "pcgamingcr" in url:
            return "pcgamingcr"
        
        elif "ticotek" in url:
            return "ticotek"
        
        else:
            return "basura"