from nltk.corpus import wordnet
from deep_translator import GoogleTranslator
import nltk

#Descarga de dependencias
#nltk.download('wordnet')
#nltk.download('omw-1.4')

class Preprocesamiento:
    lista_terminos = None

    def __init__(self, terminos):
        self.lista_terminos = terminos
    
    def tesauro(self, palabra):
        """
        Función que se encarga de obtener los tesauros
        E: Una palabra a conseguir tesauros
        S: Los tesauros
        """
        sinonimos_ingles = []

        syn = wordnet.synsets(palabra)

        for syn in wordnet.synsets(palabra):
            for lemma in syn.lemmas():
                sinonimos_ingles.append(lemma.name())

        sinonimos_traducidos = []
        
        sinonimos = ','.join([str(sinonimo) for sinonimo in list(set(sinonimos_ingles))])
        
        sinonimos_traducidos = self.traducir(sinonimos).split(",")

        return sinonimos_traducidos

    def traducir(self, palabra):
        """
        Función que se encarga de traducir
        E: Una palabra a traducir
        S: Palabra traducida
        """
        traductor = GoogleTranslator(source='en', target='es')
        return traductor.translate(palabra)
